import logo from './logo.svg';
import './App.css';


const myGitFriends = [
  { login: "OlgaZuka", avatar_url: "https://secure.gravatar.com/avatar/7cc4a3d71569123e826d26e285829879?s=800&d=identicon" },
  { login: "friend_1", avatar_url: "https://avatars0.githubusercontent.com/u/3?v=4" },
  { login: "friend_2", avatar_url: "https://avatars0.githubusercontent.com/u/5?v=4" }
]


const App = () => {
  return (
    <div className="App">
      <header className="My Git Friends">
      </header>
      { myGitFriends.map(u =>
        <div>
          <h1>{u.login}</h1>
          <img src={u.avatar_url} width={100} alt="" />
        </div>
      )}

    </div>

  );
}

export default App;
